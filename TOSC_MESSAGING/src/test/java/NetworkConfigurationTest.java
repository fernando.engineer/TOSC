import eu.systemsengineer.fjcapeletto.msg.config.NetworkConfiguration;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NetworkConfigurationTest {

    static ApplicationContext springContext = new ClassPathXmlApplicationContext("Configuration.xml");
    private static NetworkConfiguration netConfig = (NetworkConfiguration) springContext.getBean("network-configuration");

    @Test
    public void testNetworkConfiguration() {
        assertEquals(100, netConfig.getControllerWaitingTimeinMillisforUpdateTrackList());
        assertEquals(20, netConfig.getControllerWaitingTimeinMillisforLaunchPublication());
        assertEquals(2000, netConfig.getControllerDelayTimeinMillisforHearBeat());
        assertEquals("(Adjust for your neeeds in NetworkConfig.xml.)", netConfig.getNetConfigInfo());
    }

}
