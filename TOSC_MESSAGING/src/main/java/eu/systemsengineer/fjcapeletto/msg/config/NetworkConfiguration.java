package eu.systemsengineer.fjcapeletto.msg.config;

/**
 * Class containing the properties for the Network Configurations.
 * Configured through Spring Injection by the bean 'network-configuration'  
 * @author Fernando Jose Capeletto Neto
 * @see /TOSC_MESSAGING/src/main/resources/Configuration.xml
 */
public class NetworkConfiguration {

	private int controllerWaitingTimeinMillisforLaunchPublication;
	private int controllerWaitingTimeinMillisforUpdateTrackList;
	private int controllerDelayTimeinMillisforHearBeat;
	private String netConfigInfo;

	/**
	 * Returns the String message to be raised during starting application. 
	 * @return String message to be raised during starting application.
	 */	
	public String getNetConfigInfo() {
		return netConfigInfo;
	}

	/**
	 * Sets the String message to be raised during starting application. 
	 * Used for Spring Injection.
	 * @param netConfigInfo String message to be raised during starting application.
	 */	
	public void setNetConfigInfo(String netConfigInfo) {
		this.netConfigInfo = netConfigInfo;
	}

	/**
	 * Returns the time in MilliSeconds to be wait after a Launch Car Publication. 
	 * The intention is to avoid the ConcurrentModificationException that can be raised if several cars was launched at once.
	 * This exceptions occurred when I tried to launch 10 cars at once.
	 * The value was determined empirically, and depending by the network constrains, can be different.
	 * @return time in MilliSeconds to be wait after a Launch Car Publication.
	 */	
	public int getControllerWaitingTimeinMillisforLaunchPublication() {
		return controllerWaitingTimeinMillisforLaunchPublication;
	}

	/**
	 * Set the time in MilliSeconds to be wait after a Launch Car Publication. 
	 * The intention is to avoid the ConcurrentModificationException that can be raised if several cars was launched at once.
	 * This exceptions occurred when I tried to launch 10 cars at once.
	 * The value was determined empirically, and depending by the network constrains, can be different.
	 * Used for Spring Injection.
	 * @param timeinMillis time in MilliSeconds to be wait after a Launch Car Publication.
	 */	
	public void setControllerWaitingTimeinMillisforLaunchPublication(int timeinMillis) {
		this.controllerWaitingTimeinMillisforLaunchPublication = timeinMillis;
	}

	/**
	 * Returns the time in MilliSeconds to be wait after a Update Track List Publication. 
	 * The intention is to avoid the ConcurrentModificationException that can be raised because this Updating occurs inside a infinite loop in the TOSController;
	 * This exceptions never occurred but I decided to implement it for safety. 
	 * The value was determined empirically, and this value affect directly the 'speed' of the cars.
	 * @return time in MilliSeconds to be wait after a Update Track List Publication.
	 */	
	public int getControllerWaitingTimeinMillisforUpdateTrackList() {
		return controllerWaitingTimeinMillisforUpdateTrackList;
	}

	/**
	 * Set the time in MilliSeconds to be wait after a Update Track List Publication. 
	 * The intention is to avoid the ConcurrentModificationException that can be raised because this Updating occurs inside a infinite loop in the TOSController;
	 * This exceptions never occurred but I decided to implement it for safety. 
	 * The value was determined empirically, and this value affect directly the 'speed' of the cars.
	 * @param controllerWaitingTimeinMillisforUpdateTrackList time in MilliSeconds to be wait after a Update Track List Publication.
	 */	
	public void setControllerWaitingTimeinMillisforUpdateTrackList(int controllerWaitingTimeinMillisforUpdateTrackList) {
		this.controllerWaitingTimeinMillisforUpdateTrackList = controllerWaitingTimeinMillisforUpdateTrackList;
	}
	
	/**
	 * Returns the time in MilliSeconds between two consecutive Heart Beat messages.
	 * @return controllerDelayTimeinMillisforHearBeat time in MilliSeconds between two consecutive Heart Beat messages.
	 */
	public int getControllerDelayTimeinMillisforHearBeat() {
		return controllerDelayTimeinMillisforHearBeat;
	}

	
	/**
	 * Set the time in MilliSeconds between two consecutive Heart Beat messages 
	 * @param controllerDelayTimeinMillisforHearBeat time in MilliSeconds between two consecutive Heart Beat messages
	 */	
	public void setControllerDelayTimeinMillisforHearBeat(int controllerDelayTimeinMillisforHearBeat) {
		this.controllerDelayTimeinMillisforHearBeat = controllerDelayTimeinMillisforHearBeat;
	}
	
	
}
