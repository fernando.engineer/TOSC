package eu.systemsengineer.fjcapeletto.msg.publisher;

import java.util.Map;
import javax.jms.JMSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;


/**
 * Class containing the Apache ActiveMQ Publisher for the Set CourseCar Message
 * @author Fernando Jose Capeletto Neto
 */
public class PublisherSetCourseCar extends Publisher{

	private static final Logger LOGGER = LoggerFactory.getLogger(PublisherSetCourseCar.class);

	/**
	 * Prepare the connection, set the client and create the session. 
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException
	 */
	public void connect(String brokerURL) throws JMSException {
		super.connect(brokerURL,UtilsMsg.SETCOURSE_TOPIC);
	}


	/**
	 * SetCourseCar Message Method. Set a new Course for a given Car.
	 * @param	maneuveredCarsMap The Map of cars to be maneuvered
	 * @param	angleInDegrees The new course in degrees
	 * @throws JMSException
	 */
	public void publish(Map<Integer, Car> maneuveredCarsMap, float angleInDegrees) throws JMSException {
		for (Map.Entry<Integer, Car> entry : maneuveredCarsMap.entrySet()) {
			Car car = entry.getValue();
			car.addCourse(angleInDegrees);
			LOGGER.info("Publishing Order: Set course for car " + car.getCarName() + " Course: " + angleInDegrees + " degrees.");
		}
		super.publish("Set Course Car ", maneuveredCarsMap);
	}
	
}