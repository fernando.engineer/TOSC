package eu.systemsengineer.fjcapeletto.msg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class containing Abstract Controllers of the Project.
 * 
 * @author Fernando Jose Capeletto Neto
 */
public abstract class TOSController implements Runnable {
	private static final Logger LOGGER = LoggerFactory.getLogger(TOSController.class);

	private String brokerIP;
	private boolean brokerConfirmed = false;

	/**
	 * Constructor
	 *
	 */
	protected TOSController() {

	}

	/**
	 * Returns the IP for the Broker
	 * 
	 * @return brokerIP
	 */
	public String getBrokerIP() {
		return brokerIP;
	}

	/**
	 * Sets the IP for the Broker
	 * 
	 * @param brokerIP The IP for the Broker
	 */
	public void setBrokerIP(String brokerIP) {
		this.brokerIP = brokerIP;
	}

	
	/**
	 * Returns the information if the Brokers IP was already confirmed 
	 * @return brokerConfirmed
	 */
	public boolean isBrokerConfirmed() {
		return brokerConfirmed;
	}
	
	/**
	 * Sets the information if the Brokers IP was already confirmed 
	 * @param brokerConfirmed the information if the Brokers IP was already confirmed 
	 */
	public void setBrokerConfirmed(boolean brokerConfirmed) {
		this.brokerConfirmed = brokerConfirmed;
	}

	// Thread :
	public void run() {
		LOGGER.info("Abstract runner: Method shall be overriden.");
	}

}
