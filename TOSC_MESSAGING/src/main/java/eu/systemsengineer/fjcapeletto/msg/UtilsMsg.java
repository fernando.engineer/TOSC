package eu.systemsengineer.fjcapeletto.msg;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Class containing global definitions and utilities methods for use in whole project. 
 * Concentrate all Spring beans and provides it to the other classes.
 * @author Fernando Jose Capeletto Neto
 */
public final class UtilsMsg{
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilsMsg.class);
	private static final String PREFIXURLBROKER = "failover://(tcp://";
	private static final String SUFFIXURLBROKER = ":61616?trace=true)";
	public static final String HANDLERSTART_TOPIC = "handlerStart";
	public static final String HEARTBEAT_TOPIC = "heartBeat";
	public static final String LAUNCHCAR_TOPIC = "launchCar";
	public static final String SETCOURSE_TOPIC = "setCourseCar";
	public static final String UPDATECARMAP_TOPIC = "updateCarMap";
	
	// Beans Configurations
	// XML Configuration Files and its Config Beans
	private static final ApplicationContext SPRINGCONTEXT = new ClassPathXmlApplicationContext("Configuration.xml");
	public static final ActiveMQConnectionFactory CONNECTIONFACTORY = (ActiveMQConnectionFactory) SPRINGCONTEXT.getBean("jmsFactory");

	private UtilsMsg() {
	}

	public static final String getPrefixClient()  {
		String prefixClient;
		try {
			prefixClient = InetAddress.getLocalHost().toString();
		} catch (UnknownHostException e) {
			LOGGER.error("UnknownHostException:" + e );
			prefixClient = "UnknownHost";
		}
		return prefixClient+":";
	}
	
	
	public static final String checkBrokerURL(String brokerURL) throws NonURLException {
		InetAddressValidator validator = InetAddressValidator.getInstance();
		if(validator.isValidInet4Address(brokerURL)) {
			return PREFIXURLBROKER+brokerURL+SUFFIXURLBROKER;
		}
		else {
			throw new NonURLException(brokerURL);

		}
	}
	
	
	public static class NonURLException extends Exception
	{
		static final String NOT_A_VALID_URL = "Informed value is not a valid URL";
		public NonURLException() {}
		public NonURLException( String str )
		{
			super(NOT_A_VALID_URL + " " + str);
		}
	}
	
}
