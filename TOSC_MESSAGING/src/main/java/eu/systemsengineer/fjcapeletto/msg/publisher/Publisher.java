package eu.systemsengineer.fjcapeletto.msg.publisher;

import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.Serializable;

/**
 * Class containing the Abstract Apache ActiveMQ Publisher
 * @author Fernando Jose Capeletto Neto
 */
public abstract class Publisher {
	private static final Logger LOGGER = LoggerFactory.getLogger(Publisher.class);
	protected static final String CLIENT_ID_PREFIX = UtilsMsg.getPrefixClient()+"publisher-";
	private Connection connection;
	private Session session;
	private MessageProducer messageProducer;

	/**
	 * Prepare the connection, set the client and create the session. 
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException 
	 */
	public void connect(String brokerURL, String topicName) throws JMSException {

		// create a Connection Factory
		ActiveMQConnectionFactory connectionFactory = UtilsMsg.CONNECTIONFACTORY;
		try {
			String externalBroker = UtilsMsg.checkBrokerURL(brokerURL);
			connectionFactory.setBrokerURL(externalBroker);
		} catch (Exception e) {
			LOGGER.error("Exception" + e);
			LOGGER.error("Default broker will be used: " + UtilsMsg.CONNECTIONFACTORY.getBrokerURL());
		}
		
		// create a Connection
		connection = connectionFactory.createConnection();
		connection.setClientID(Publisher.CLIENT_ID_PREFIX+topicName);

		// create a Session
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		// create the Topic to which messages will be sent
		Topic topic = session.createTopic(topicName);

		// create a MessageProducer for sending messages
		messageProducer = session.createProducer(topic);
	}

	/**
	 * Close the connection 
	 * @throws JMSException
	 */
	public void closeConnection() throws JMSException {
		connection.close();
	}

	/**
	 * handlerStart Message Method. Alerts display that the Server was Started.
	 * In case of Display was started before Server, or due to the absence of the Server and its returns, Display situation will be reset when receives this message. 
	 * @throws JMSException
	 */
	public void publish(String description, Object obj) throws JMSException {
		LOGGER.info(description + "Publishing Message");
		ObjectMessage messageobj = session.createObjectMessage();
		messageobj.setObject((Serializable) obj);
		// send the message to the topic destination
		messageProducer.send(messageobj);
		LOGGER.info(description + "Published Message: ["+ messageobj + "]");

	}
}