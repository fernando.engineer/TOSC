package eu.systemsengineer.fjcapeletto.msg.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Class containing global definitions and utilities methods for use in whole project. 
 * Concentrate all Spring beans and provides it to the other classes.
 * @author Fernando Jose Capeletto Neto
 */
public class NetworkLayerLoader {

	private NetworkLayerLoader() {
	}

	// Beans Configurations
	// XML Configuration Files and its Config Beans
	private static ApplicationContext springContext = new ClassPathXmlApplicationContext("Configuration.xml");
	private static NetworkConfiguration netConfig = (NetworkConfiguration) springContext.getBean("network-configuration");

	// Concentrates all the properties injected by beans in utilities attributes ...

	//.. for Network Configuration
	public static final int CONTROLLER_DELAYTIME_INMILLIS_FOR_LAUNCHPUBLICATION = netConfig.getControllerWaitingTimeinMillisforLaunchPublication();
	public static final int CONTROLLER_DELAYTIME_INMILLIS_FOR_UPDATETACTICALSITUATION = netConfig.getControllerWaitingTimeinMillisforUpdateTrackList();
	public static final int CONTROLLER_DELAYTIME_INMILLIS_FOR_HEARTBEAT = netConfig.getControllerDelayTimeinMillisforHearBeat();
	public static final String NETCONFIGINFO = netConfig.getNetConfigInfo();


}
