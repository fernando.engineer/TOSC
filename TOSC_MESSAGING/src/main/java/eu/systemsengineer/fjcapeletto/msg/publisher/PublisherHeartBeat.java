package eu.systemsengineer.fjcapeletto.msg.publisher;

import javax.jms.JMSException;
import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;

/**
 * Class containing the Apache ActiveMQ Publisher for the HeartBeat Message
 * @author Fernando Jose Capeletto Neto
 */
public class PublisherHeartBeat extends Publisher{

	/**
	 * Prepare the connection, set the client and create the session. 
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException 
	 */
	public void connect(String brokerURL) throws JMSException {
		super.connect(brokerURL, UtilsMsg.HEARTBEAT_TOPIC);
	}

	/**
	 * publish method for heartBeat Message Method. Keeps display informed about the presence of the Server.
	 * This message is sent with a periodicity defined by Spring configuration. And in case of absence of this message for n cycles, The Display will considers that Server is down.)
	 * In This case, since this unavailability can be due to the network failures (and not only about server termination),
	 * the absence of this message will not reset the scenario, but only blocking the controls and alerts the operator. (Allowing the possibility of server returns without lose the scenario).
	 * @throws JMSException
	 */
	public void publish() throws JMSException {
		super.publish("Heart Beat ", true);
	}
}