package eu.systemsengineer.fjcapeletto.msg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;


/**
 * Class containing the BROKER IP MMI CONFIRMATION
 * 
 * @author Fernando Jose Capeletto Neto
 */

@SuppressWarnings("serial")
public class BrokerIPPanel extends JPanel  {
	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerIPPanel.class);
	private JFrame frameBrokerIP = new JFrame("Terminal Operation System Challenge - A Project-Challenge for Fernando Jose Capeletto Neto application to TBA Group");
	private JButton buttonEnterIP;
	private JTextField fieldIP;
	private JLabel labelIP = new JLabel("Type Broker's IP or leave empty to default connection: " + UtilsMsg.CONNECTIONFACTORY.getBrokerURL());

	/**
	 * Constructor
	 */
	public BrokerIPPanel(TOSController instanceController) {
		labelIP.setBounds(10, 10,600, 20);
		labelIP.setHorizontalAlignment(SwingConstants.LEFT);
		labelIP.setEnabled(true);
		add(labelIP);
		
		fieldIP = new JTextField();
		fieldIP.setBounds(610, 10,100, 20);
		fieldIP.setHorizontalAlignment(SwingConstants.LEFT);
		fieldIP.setEnabled(true);
		add(fieldIP);
		fieldIP.addActionListener((ActionEvent e) -> confirmBroker(instanceController));

		
		buttonEnterIP = new JButton(new AbstractAction("OK") {
			public void actionPerformed(ActionEvent e) {
				confirmBroker(instanceController);
				}
		});
		buttonEnterIP.setBounds(720,10,60,20);
		buttonEnterIP.setHorizontalAlignment(SwingConstants.LEFT);
		buttonEnterIP.setEnabled(true);
		setLayout(null);
		add(buttonEnterIP);
	}

	/**
	 * Set the dimension for the application
	 */
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(800, 50);
	}

	/**
	 * Returns the Frame for the Panel Broker IP 
	 * @return frame
	 */
	public JFrame getFrame() {
		return frameBrokerIP;
	}
	
	/**
	 * Confirm the Broker and disposes the frame. 
	 */
	public void confirmBroker(TOSController instanceController) {
		instanceController.setBrokerIP(fieldIP.getText());
		instanceController.setBrokerConfirmed(true);
		setEnabled(false);
		LOGGER.info("Broker entered: " + fieldIP.getText());
		frameBrokerIP.dispose();
	}
	
	/**
	 * App method, just call the SwingUtilities
	 */
	public void app(TOSController instanceController) {
		SwingUtilities.invokeLater(() -> new BrokerIPPanel(instanceController).go(instanceController));
	}


	/**
	 * Assembly the frame and start it
	 */
	public void go(TOSController instanceController) {
		frameBrokerIP.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frameBrokerIP.getContentPane().add(this);
		frameBrokerIP.setResizable(false);
		frameBrokerIP.pack();
		frameBrokerIP.setLocationByPlatform(true);
		frameBrokerIP.setVisible(true);
		
		frameBrokerIP.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				instanceController.setBrokerConfirmed(true);
		    	setEnabled(false);
		    }
		});
		

	}
}
