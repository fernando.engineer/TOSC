package eu.systemsengineer.fjcapeletto.msg.publisher;


import java.util.Map;
import javax.jms.JMSException;
import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;

/**
 * Class containing the Apache ActiveMQ Publisher for the UpdateCarList Message
 * @author Fernando Jose Capeletto Neto
 */
public class PublisherUpdateCarList  extends Publisher{

	/**
	 * Prepare the connection, set the client and create the session.
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException
	 */
	public void connect(String brokerURL) throws JMSException {
		super.connect(brokerURL, UtilsMsg.UPDATECARMAP_TOPIC);
	}

	/**
	 * updateCarList Message Method. Send the Map of Cars updated.
	 * @param	launchedCarsMap The updated Maps of cars
	 * @throws JMSException
	 */
	public void publish(Map<Integer, Car> launchedCarsMap) throws JMSException {
		super.publish("Updated Car ", launchedCarsMap);
	}
}