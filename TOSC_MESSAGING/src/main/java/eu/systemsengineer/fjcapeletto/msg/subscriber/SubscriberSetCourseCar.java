package eu.systemsengineer.fjcapeletto.msg.subscriber;

import java.util.Map;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.msg.UtilsMsg;


/**
 * Class containing the Apache ActiveMQ Subscriber for the Set CourseCar Message
 * @author Fernando Jose Capeletto Neto
 */
public class SubscriberSetCourseCar extends Subscriber {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriberSetCourseCar.class);

	/**
	 * Prepare the conection, set the client and create the session. 
	 * @param brokerURL The URL for the broker service
	 * @throws JMSException
	 */
	public void connect(String brokerURL) throws JMSException {
		super.connect(brokerURL,UtilsMsg.SETCOURSE_TOPIC );
	}

	/**
	 * OnMessage Method. 
	 * When the message arrives, cast it to a CarCourse object. 
	 * @param	message The message
	 */
	@Override
	public void onMessage(Message message) {
		LOGGER.debug("onMessage " + message);
		if (message instanceof ObjectMessage) {
			ObjectMessage objMsg = (ObjectMessage) message;
			try {
				// When the message arrives, try cast it to a Map<Integer, Car> object.
				Map<Integer, Car> maneuveredCarsMap = (Map<Integer, Car>) objMsg.getObject();
				for (Map.Entry<Integer, Car> entry : maneuveredCarsMap.entrySet()) {
					Car car = entry.getValue();
					LOGGER.info("Changed Course for Car " + car.getCarName() + " Course: "
							+ car.getLastCourse() + " degrees.");
				}
			} catch (JMSException e) {
				LOGGER.debug("Not a Map<Integer, Car> obj: " + e);
			}
		}

		
	}
}