package eu.systemsengineer.fjcapeletto.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import eu.systemsengineer.fjcapeletto.model.config.TerminalLoader;
import eu.systemsengineer.fjcapeletto.msg.TOSController;
import eu.systemsengineer.fjcapeletto.msg.config.NetworkLayerLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.msg.publisher.PublisherHandlerStart;
import eu.systemsengineer.fjcapeletto.msg.publisher.PublisherHeartBeat;
import eu.systemsengineer.fjcapeletto.msg.publisher.PublisherUpdateCarList;
import eu.systemsengineer.fjcapeletto.msg.subscriber.SubscriberLaunchCar;
import eu.systemsengineer.fjcapeletto.msg.subscriber.SubscriberSetCourseCar;

/**
 * Class containing the Controller of the Project. Acts between display and
 * model. Contains the Publisher and Subscriber implementation for the Async
 * Messages. Consumes the Launch Car and SetCourse Car Async Messages, Update
 * the model and Publish the UpdateCar Async Message constantly.
 * 
 * @author Fernando Jose Capeletto Neto
 */
public class TOSHandlerController extends TOSController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TOSHandlerController.class);
	// Singletown Class, only one instance.
	private static final TOSHandlerController instance = new TOSHandlerController();

	// Publishers and Subscribers attributes
	private static PublisherHeartBeat publisherHeartBeat;
	
	// Used to compute the next position taken into account the Car Speed.
	double deltaX;
	double deltaY = 0;

	// Controls the launched cars
	private List<Car> tracksList = Collections.synchronizedList(new ArrayList<Car>());
	private Map<Integer, Car> launchedCarsMap = new HashMap<>();
	
	//For a future version.. (to be used in the avoid colision detection)
	// TODO: private Map<Integer, Integer> occupiedXPositions = new HashMap<Integer, Integer>()
	// TODO: private Map<Integer, Integer> occupiedYPositions = new HashMap<Integer, Integer>()
	
	private int totalCarsLaunched = 0;

	public static TOSHandlerController getInstance() {
		return instance;
	}

	/**
	 * Constructor
	 * 
	 */
	private TOSHandlerController() {
		super();
	}

	/**
	 * Returns the Map for the launched Cars
	 * 
	 * @return launchedCarsMap
	 */
	public Map<Integer, Car> getLaunchedCarsMap() {
		return launchedCarsMap;
	}

	/**
	 * Returns the total of cars launched
	 * 
	 * @return totalCarsLaunched
	 */
	public int getTotalCarsLaunched() {
		return totalCarsLaunched;
	}

	Thread publisherHeartBeatThread = new Thread(() -> {
			publisherHeartBeat = new PublisherHeartBeat();
			try {
				// Try to connect to the client and sets the topic for the message
				publisherHeartBeat.connect(getBrokerIP());
			} catch (JMSException e) {
				LOGGER.error("Exception connecting to PublisherHeartBeat:" + e);
			}
			while (true) {
				try {
					// Sends a message to alert Display about Server presence
					publisherHeartBeat.publish();
					LOGGER.info("Publish Message to Alert about Server presence.");
					Thread.sleep(NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_HEARTBEAT);
				} catch (JMSException e) {
					LOGGER.error("Exception publishing PublisherHeartBeat:" + e);
				} catch (InterruptedException e) {
					LOGGER.error("InterruptedException:" + e);
					Thread.currentThread().interrupt();
				}
			}
	});

	@Override
	public void run() {

		// Start thread for HeartBeat Publisher
		publisherHeartBeatThread.start();
		
        startPublishHandler();
		// Create a instance of UpdateCarList Publisher Message
		PublisherUpdateCarList publisherUpdateTrackList = new PublisherUpdateCarList();
		try {
			// Try to connect to the client and sets the topic for the message
			publisherUpdateTrackList.connect(getBrokerIP());
		} catch (JMSException e) {
			LOGGER.error("Exception connecting to PublisherUpdateCarList :" + e);
		}
		// Create a instance of LaunchCar Subscriber Message
		SubscriberLaunchCar subscriberLaunchCar = new SubscriberLaunchCar();
		try {
			// Try to connect to the client and sets the topic for the message
			subscriberLaunchCar.connect(getBrokerIP());
			// Listens to the message..
			subscriberLaunchCar.getMessageConsumer().setMessageListener(this::processLaunchCarMessage);
		} catch (JMSException e) {
			LOGGER.error("Exception receiving SubscriberLaunchCar :" + e);
		}

		// Create a instance of SetCourseCar Subscriber Message
		SubscriberSetCourseCar subscribeSetCourseCar = new SubscriberSetCourseCar();
		try {
			// Try to connect to the client and sets the topic for the message
			subscribeSetCourseCar.connect(getBrokerIP());
			// Listens to the message..
			subscribeSetCourseCar.getMessageConsumer().setMessageListener(this::processSetCourseCarMessage);
		} catch (JMSException e) {
			LOGGER.error("JMSException:" + e);
		}

		// During the infinite loop:
		while (true) {
			// For each car..
			for (Map.Entry<Integer, Car> entry : launchedCarsMap.entrySet()) {
				Car car = entry.getValue();
				// Computes the next position
				deltaX = (car.getLastSpeed() * Math.cos((90 - car.getLastCourse()) * Math.PI / 180));
				deltaY = -(car.getLastSpeed() * Math.sin((90 - car.getLastCourse()) * Math.PI / 180));

				// Set the next position
				car.setLastLatitude(car.getLastLatitude() + deltaY);
				car.setLastLongitude(car.getLastLongitude() + deltaX);

				// Avoid Colision System method disabled : See comments bellow on the method.
				// (An Extra for future version!)
				// TODO: avoidColisionSystem(car)

				// Detect if reaches the terminal border and 'reflect' its course
				reflectAtBorder(car);
				//Updates the positions occupied to prevent colision
				//TODO: occupiedXPositions.put(car.getCarId(), (int) car.getLastLongitude())
				//TODO: occupiedYPositions.put(car.getCarId(), (int) car.getLastLatitude())
			}

			try {
				// After updates the position, publishes a message containing the list of cars.
				publisherUpdateTrackList.publish(launchedCarsMap);
				LOGGER.trace("UpdateTrackListRequest method requested update of track list.");
				LOGGER.trace(
						"Waiting for " + NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_UPDATETACTICALSITUATION + "ms");

				// Just for safety since this publications is under an infinite loop. See the
				// comments in the xml network configuration
				Thread.sleep(NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_UPDATETACTICALSITUATION);
			} catch (JMSException e) {
				LOGGER.error("JMSException:" + e);
			} catch (InterruptedException e) {
				LOGGER.error("InterruptedException:" + e);
				Thread.currentThread().interrupt();
			}
		}
	}

	private void startPublishHandler() {
		// Create a instance of PublisherHandlerStart Publisher Message
		PublisherHandlerStart publisherHandlerStart = new PublisherHandlerStart();
		try {
			// Try to connect to the client and sets the topic for the message
			publisherHandlerStart.connect(getBrokerIP());
			// Sends a message to alert Display about Server initialiation
			publisherHandlerStart.publish();
			LOGGER.trace("Publish Message to Alert about Server Initialization.");
		} catch (JMSException e) {
			LOGGER.error("Exception publishing publisherHandlerStart:" + e);
		}
	}

	private void processLaunchCarMessage(Message message) {
		LOGGER.debug("Received Message: [" + message + "]");
		if (message instanceof ObjectMessage) {
			LOGGER.info("Received Object Message (List<Car> Type): [" + message + "]");
			ObjectMessage objMsg = (ObjectMessage) message;
			try {
				// When the message arrives, try cast it to a List<Car> object.
				List<Car> listCarToLaunch = (List<Car>) objMsg.getObject();
				// Add the track to the permanent control.
				tracksList.addAll(listCarToLaunch);
				for (Iterator<Car> iterator = listCarToLaunch.iterator(); iterator.hasNext();) {
					Car carLauched = iterator.next();
					launchedCarsMap.put(carLauched.getCarId(), carLauched);
					LOGGER.info("Publishing and adding to the active List of Cars: Car "
							+ carLauched.getCarName() + ": Speed: " + carLauched.getLastSpeed()
							+ " units/cycle [Lat/Long]:(" + carLauched.getLastLatitude() + ","
							+ carLauched.getLastLongitude() + ") " + "Course: " + carLauched.getLastCourse()
							+ " Color: " + carLauched.getCarColor());
				}
				totalCarsLaunched += listCarToLaunch.size();
			} catch (JMSException e) {
				LOGGER.error("JMSException: Error during Track creation: " + e);
			}
		}
	}

	private void processSetCourseCarMessage(Message message) {
		LOGGER.debug("Received Message: [" + message + "]");
		if (message instanceof ObjectMessage) {
			LOGGER.info("Received Object Map<Integer, Car> for Set Cars Courses): [" + message + "]");
			ObjectMessage objMsg = (ObjectMessage) message;
			try {
				// When the message arrives, try cast it to a Map<Integer, Car> object.
				Map<Integer, Car> maneuveredCarsMap = (Map<Integer, Car>) objMsg.getObject();
				for (Map.Entry<Integer, Car> entry : maneuveredCarsMap.entrySet()) {
					Car car = entry.getValue();
					launchedCarsMap.get(car.getCarId()).addCourse(car.getLastCourse());
					LOGGER.info("Changed Course for Car " + car.getCarName() + " Course: "
							+ car.getLastCourse() + " degrees.");
				}
				LOGGER.info("Consumed Map<Integer, Car> Message for Set Cars Courses)");
			} catch (JMSException e) {
				LOGGER.error("JMSException: Error during Seting Course of Cars: " + e);
			}
		}
	}

	// Extra: I tried to implemented an avoid colision system as an Extra, but it
	// raised a lot of ConcurrentModificationExceptions.
	// I need more time to thinking about a solution for this. Due to the time limit
	// to deliver the challenge, I prefer to leave this extra for another
	// opportunity
	// For fun: You can enable the method and see that sometimes works well,
	// sometimes the cars enter in clash..
	/**
	 * Avoid Collision System Method. Try to detect the crashes and revert the
	 * course of the cars envolved.
	 * @param car The car to be checked if is in course to collide
	 */
	@SuppressWarnings("Future Implementation")
	private void avoidColisionSystem(Car car) {
		// For each car, compares its position with the occupied positions by anothers cars to prevent colision and changes courses accordlng
		for (Iterator<Car> iterator = tracksList.iterator(); iterator.hasNext();) {
			Car othercar = iterator.next();
			if (((car.getLastLatitude() < tracksList.get(othercar.getCarId()).getLastLatitude() + car.getCarVerticalSize() + TerminalLoader.CARMARGIN)
					&& (car.getLastLatitude() > tracksList.get(othercar.getCarId()).getLastLatitude() - car.getCarVerticalSize() - TerminalLoader.CARMARGIN))
					&& 
					((car.getLastLongitude() < tracksList.get(othercar.getCarId()).getLastLongitude() + car.getCarVerticalSize() + TerminalLoader.CARMARGIN)
							&& (car.getLastLongitude() > tracksList.get(othercar.getCarId()).getLastLongitude() - car.getCarVerticalSize() - TerminalLoader.CARMARGIN))) {
				LOGGER.info("Car " + car.getCarName() + " is going to crash with Car " + tracksList.get(othercar.getCarId()).getCarName() + " Both cars will revert its courses");
				// Change to the opposite course!
				// Since this leads to 2 Async messages, and sometimes the cars enter in clash.
				// This method would crash the whole App with an ConcurrentModification
				// exception.
				car.setLastCourse(-car.getLastCourse());
				othercar.setLastCourse(-othercar.getLastCourse());
			}
		}
	}

	/**
	 * Reflect at Border Method. Compares the new position of the car with the
	 * terminal borders, and if reaches the limits, 'reflects' its course. (as Snell
	 * law of reflection, just to keep the cars in movement!)
	 * 
	 * @param track The car to be checked if is in the border
	 */
	private void reflectAtBorder(Car track) {
		if (!((track.getLastLatitude() >= TerminalLoader.TERMINALBORDER)
				&& (track.getLastLatitude() <= TerminalLoader.TERMINALVERTICALSIZE - TerminalLoader.TERMINALBORDER
						- TerminalLoader.CARMARGIN))) {
			track.addCourse(180 - track.getLastCourse());
			if (track.getLastLatitude() < TerminalLoader.TERMINALBORDER)
				track.setLastLatitude(TerminalLoader.TERMINALBORDER);
			if (track.getLastLatitude() > TerminalLoader.TERMINALVERTICALSIZE - TerminalLoader.TERMINALBORDER
					- TerminalLoader.CARMARGIN)
				track.setLastLatitude(
						(double) TerminalLoader.TERMINALVERTICALSIZE - TerminalLoader.TERMINALBORDER - TerminalLoader.CARMARGIN);
		}
		if (!((track.getLastLongitude() >= TerminalLoader.TERMINALBORDER + TerminalLoader.TERMINALSTARTHORIZONTALPOSITION)
				&& (track.getLastLongitude() <= TerminalLoader.TERMINALHORIZONTALSIZE - TerminalLoader.TERMINALBORDER
						- TerminalLoader.CARMARGIN))) {
			track.addCourse(360 - track.getLastCourse());
			if (track.getLastLongitude() < TerminalLoader.TERMINALBORDER + TerminalLoader.TERMINALSTARTHORIZONTALPOSITION){
				track.setLastLongitude((double) TerminalLoader.TERMINALBORDER + TerminalLoader.TERMINALSTARTHORIZONTALPOSITION);
			}
			if (track.getLastLongitude() > TerminalLoader.TERMINALHORIZONTALSIZE - TerminalLoader.TERMINALBORDER
					- TerminalLoader.CARMARGIN)
				track.setLastLongitude(
						(double) TerminalLoader.TERMINALHORIZONTALSIZE - TerminalLoader.TERMINALBORDER - TerminalLoader.CARMARGIN);
		}
	}

}
