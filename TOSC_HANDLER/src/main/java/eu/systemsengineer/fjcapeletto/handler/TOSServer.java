package eu.systemsengineer.fjcapeletto.handler;

import eu.systemsengineer.fjcapeletto.msg.BrokerIPPanel;
import eu.systemsengineer.fjcapeletto.msg.config.NetworkLayerLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Entrypoint for the App containing the main classe for the TOC-MK2-V1 project
 * @author Fernando Jose Capeletto Neto
 */
public class TOSServer {
	private static final Logger LOGGER = LoggerFactory.getLogger(TOSServer.class);
	static String brokerIP;

	// Instance of the Controller
	private TOSHandlerController instanceMSGReader;

	/**
	 * Constructor   
	 * 
	 */		
	public TOSServer() {
		// Some informations about network configuratios. (Please see the xml file)
		LOGGER.info("controllerWaitingTimeinMillisforLaunchPublication: " + NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_LAUNCHPUBLICATION + NetworkLayerLoader.NETCONFIGINFO);
		LOGGER.info("controllerWaitingTimeinMillisforUpdateTrackList: " + NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_UPDATETACTICALSITUATION + NetworkLayerLoader.NETCONFIGINFO);
		LOGGER.info("controllerDelayTimeinMillisforHearBeat: " + NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_HEARTBEAT + NetworkLayerLoader.NETCONFIGINFO);
	}

	/**
	 * Main Class : Starts the Display, get a instance of the Controller and start it.   
	 */
	public static void main(String[] args) {
		TOSServer server = new TOSServer();
		server.instanceMSGReader = TOSHandlerController.getInstance();
		server.instanceMSGReader.setBrokerConfirmed(true);
		server.instanceMSGReader.run();
	}

}



