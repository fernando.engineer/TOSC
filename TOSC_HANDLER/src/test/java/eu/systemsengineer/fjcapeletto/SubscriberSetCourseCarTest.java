package eu.systemsengineer.fjcapeletto;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.model.CarsBase;
import eu.systemsengineer.fjcapeletto.msg.publisher.PublisherSetCourseCar;
import eu.systemsengineer.fjcapeletto.msg.subscriber.SubscriberSetCourseCar;

public class SubscriberSetCourseCarTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriberSetCourseCarTest.class);
	static ApplicationContext springContext = new ClassPathXmlApplicationContext("FleetConfiguration.xml");
	static CarsBase carsBaseConfig = (CarsBase) springContext.getBean("carsbase-configuration");
	private static PublisherSetCourseCar publisherSetCourseCar;
	private static SubscriberSetCourseCar subscriberSetCourseCar;
	public static List<Car> fleetCars =  carsBaseConfig.getFleetCars();
	private Map<Integer, Car> mapCarList = new HashMap<Integer, Car>();
	private static BrokerConfig brokerconfigIntance = BrokerConfig.getInstance();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		brokerconfigIntance.start();
		publisherSetCourseCar = new PublisherSetCourseCar();
		publisherSetCourseCar.connect(null);

		subscriberSetCourseCar = new SubscriberSetCourseCar();
		subscriberSetCourseCar.connect(null);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		publisherSetCourseCar.closeConnection();
		subscriberSetCourseCar.closeConnection();
	}

	@Test
	public void testSetCourseCar() {
		try {
			Map<Integer, Car> maneuveredCarsMap  = new HashMap<Integer, Car>();
			maneuveredCarsMap.put(3, fleetCars.get(3));
			publisherSetCourseCar.publish(maneuveredCarsMap,48);
			subscriberSetCourseCar.getMessageConsumer().setMessageListener(new MessageListener() {
				@Override
				public void onMessage(Message message) {
					LOGGER.debug("onMessage " + message);
					if (message instanceof ObjectMessage) {
						ObjectMessage objMsg = (ObjectMessage) message;
						try {
							Map<Integer, Car> anothermaneuveredCarsMap = (Map<Integer, Car>) objMsg.getObject();
							Car car = anothermaneuveredCarsMap.get(3);
							assertEquals(48, car.getLastCourse(), 0.001);
						} catch (JMSException e) {
							LOGGER.error("JMSException:" + e);
						}
					}

				}
			});

		} catch (JMSException e) {
			LOGGER.error("JMSException:" + e);
		}

	}
}