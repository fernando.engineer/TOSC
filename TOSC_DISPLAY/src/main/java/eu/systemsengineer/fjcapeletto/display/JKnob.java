package eu.systemsengineer.fjcapeletto.display;
// Imports for the GUI classes.
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * JKnob.java - 
 *   A knob component.  The knob can be rotated by dragging 
 *   a spot on the knob around in a circle.
 *   The knob will report its position in radians when asked.
 *
 * @author Grant William Braught
 * @author Dickinson College
 * @version 12/4/2000
 * @see 'http://users.dickinson.edu/~braught/courses/cs132f00/classes/code/JKnob.src.html' (URL Info added by Fernando Jose Capeletto Neto)
 * @version 18/3/2020 by Fernando Jose Capeletto Neto: Implemented the 'getAngleInDegrees ()' method and adapted the size to my purposes. 
 */

class JKnob 
    extends JComponent
    implements MouseListener, MouseMotionListener {

    private static final int RADIUS = 40;
    private static final int SPOT_RADIUS = 5;

    private double theta;
    private Color knobColor;
    private Color spotColor;

    private boolean pressedOnSpot;

    /**
     * No-Arg constructor that initializes the position
     * of the knob to 0 radians (Up).
     */
    public JKnob() {
	this(0);
    }

    /**
     * Constructor that initializes the position
     * of the knob to the specified angle in radians.
     *
     * @param initTheta the initial angle of the knob.
     */
    public JKnob(double initTheta) {
	this(initTheta, Color.gray, Color.black);
    }
    
    /**
     * Constructor that initializes the position of the
     * knob to the specified position and also allows the
     * colors of the knob and spot to be specified.
     *
     * @param initTheta the initial angle of the knob.
     * @param initKnobColor the color of the knob.
     * @param initSpotColor the color of the spot.
     */
    public JKnob(double initTheta, Color initKnobColor, 
		 Color initSpotColor) {

	theta = initTheta;
	pressedOnSpot = false;
	knobColor = initKnobColor;
	spotColor = initSpotColor;

	this.addMouseListener(this);	
	this.addMouseMotionListener(this);
    }

    /**
     * Paint the JKnob on the graphics context given.  The knob
     * is a filled circle with a small filled circle offset 
     * within it to show the current angular position of the 
     * knob.
     *
     * @param g The graphics context on which to paint the knob.
     */
    @Override
    public void paint(Graphics g) {

	// Draw the knob.
	g.setColor(knobColor);
	g.fillOval(0,0,2* RADIUS,2* RADIUS);

	// Find the center of the spot.
	Point pt = getSpotCenter();
	int xc = (int)pt.getX();
	int yc = (int)pt.getY();

	// Draw the spot.
	g.setColor(spotColor);
	g.fillOval(xc- SPOT_RADIUS, yc- SPOT_RADIUS,
		   2* SPOT_RADIUS, 2* SPOT_RADIUS);
    }

    /**
     * Return the ideal size that the knob would like to be.
     *
     * @return the preferred size of the JKnob.
     */
    @Override
    public Dimension getPreferredSize() {
	return new Dimension(2* RADIUS,2* RADIUS);
    }

    /**
     * Return the minimum size that the knob would like to be.
     * This is the same size as the preferred size so the
     * knob will be of a fixed size.
     *
     * @return the minimum size of the JKnob.
     */
    @Override
    public Dimension getMinimumSize() {
	return new Dimension(2* RADIUS,2* RADIUS);
    }

    /**
     * Get the current angular position of the knob.
     *
     * @return the current angular position of the knob.
     */
    public double getAngle() {
	return theta;
    }
    
    /**
     * Get the current angular position (in degrees) of the knob.
     * @return the current angular position (in degrees) of the knob.
     */
	public double getAngleInDegrees () {
		return theta * 180 / Math.PI;
	}
	

    /** 
     * Calculate the x, y coordinates of the center of the spot.
     *
     * @return a Point containing the x,y position of the center
     *         of the spot.
     */ 
    private Point getSpotCenter() {

	// Calculate the center point of the spot RELATIVE to the
	// center of the of the circle.

	int r = RADIUS - SPOT_RADIUS;

	int xcp = (int)(r * Math.sin(theta));
	int ycp = (int)(r * Math.cos(theta));

	// Adjust the center point of the spot so that it is offset
	// from the center of the circle.  This is necessary becasue
	// 0,0 is not actually the center of the circle, it is  the 
        // upper left corner of the component!
	int xc = RADIUS + xcp;
	int yc = RADIUS - ycp;

	// Create a new Point to return since we can't  
	// return 2 values!
	return new Point(xc,yc);
    }

    /**
     * Determine if the mouse click was on the spot or
     * not.  If it was return true, otherwise return 
     * false.
     *
     * @return true if x,y is on the spot and false if not.
     */
    private boolean isOnSpot(Point pt) {
	return (pt.distance(getSpotCenter()) < SPOT_RADIUS);
    }

    // Methods from the MouseListener interface.

    /**
     * @param e reference to a MouseEvent object describing 
     *          the mouse click.
     */
    public void mouseClicked(MouseEvent e) {
        /**
         * Empy method because nothing happens on a click.
         */
    }

    /**
     * @param e reference to a MouseEvent object describing
     *          the mouse entry.
     */
    public void mouseEntered(MouseEvent e) {
        /**
         * Empty method because nothing happens when the mouse
         * enters the Knob.the mouse entry.
         */
    }

    /**
     * @param e reference to a MouseEvent object describing
     *          the mouse exit.
     */
    public void mouseExited(MouseEvent e) {
        /**
         * Empty method because nothing happens when the mouse
         * exits the knob.
         */
    }

    /**
     * When the mouse button is pressed, the dragging of the
     * spot will be enabled if the button was pressed over
     * the spot.
     *
     * @param e reference to a MouseEvent object describing
     *          the mouse press.
     */
    public void mousePressed(MouseEvent e) {

	Point mouseLoc = e.getPoint();
	pressedOnSpot = isOnSpot(mouseLoc);
    }

    /**
     * When the button is released, the dragging of the spot
     * is disabled.
     *
     * @param e reference to a MouseEvent object describing
     *          the mouse release.
     */
    public void mouseReleased(MouseEvent e) {
	pressedOnSpot = false;
    }
    
    // Methods from the MouseMotionListener interface.

    /**
     * @param e reference to a MouseEvent object describing
     *          the mouse move.
     */
    public void mouseMoved(MouseEvent e) {
        /**
         * Empty method because nothing happens when the mouse
         * is moved if it is not being dragged.
         */
    }

    /**
     * Compute the new angle for the spot and repaint the 
     * knob.  The new angle is computed based on the new
     * mouse position.
     * Update by Fernando Jose Capeletto Neto: The component is repainted only if is enable.
     * @param e reference to a MouseEvent object describing
     *          the mouse drag.
     */
    public void mouseDragged(MouseEvent e) {
	    if (pressedOnSpot && this.isEnabled() ) {
		    int mx = e.getX();
		    int my = e.getY();

		    // Compute the x, y position of the mouse RELATIVE
		    // to the center of the knob.
		    int mxp = mx - RADIUS;
		    int myp = RADIUS - my;

		    // Compute the new angle of the knob from the
		    // new x and y position of the mouse.  
		    // Math.atan2(...) computes the angle at which
		    // x,y lies from the positive y axis with cw rotations
		    // being positive and ccw being negative.
		    theta = Math.atan2(mxp, myp);
		    repaint();
		
	    }
    }

    /**
     * Here main is used simply as a test method.  If this file
     * is executed "java JKnob" then this main() method will be
     * run.  However, if another file uses a JKnob as a component
     * and that file is run then this main is ignored.
     */
    public static void main(String[] args) {

	JFrame myFrame = new JFrame("JKnob Test method");
	
	Container thePane = myFrame.getContentPane();

	// Add a JKnob to the pane.
	thePane.add(new JKnob());
    myFrame.addWindowListener(new WindowAdapter() {
            @Override
             public void windowClosing(WindowEvent e) {
                 System.exit(0);
             }
         });

	myFrame.pack();
	myFrame.setVisible(true);
    }
}