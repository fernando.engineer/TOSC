package eu.systemsengineer.fjcapeletto.display;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.swing.*;

import eu.systemsengineer.fjcapeletto.model.config.FleetConfiguration;
import eu.systemsengineer.fjcapeletto.model.config.TerminalLoader;
import eu.systemsengineer.fjcapeletto.msg.BrokerIPPanel;
import eu.systemsengineer.fjcapeletto.msg.config.NetworkLayerLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.msg.subscriber.SubscriberHandlerStart;
import eu.systemsengineer.fjcapeletto.msg.subscriber.SubscriberUpdateCarList;

/**
 * Class containing the MMI for the project. Based on Swing AWT
 * @author Fernando Jose Capeletto Neto
 */
public class TOSDisplay extends JPanel implements Runnable {
	private static final Logger LOGGER = LoggerFactory.getLogger(TOSDisplay.class);

	// TOS Controller Instantiation
	private static TOSDisplayController instanceController = TOSDisplayController.getInstance();

	// Map of Launched Cars
	transient Map<Integer, Car> launchedCarsMapFromController = instanceController.getLaunchedCarsMap();

	// Broker IP to be informed by the user during the initialization.
	private static String brokerIP;

	// Collections of visual elements for the List of Tracks.
	private Map<Integer, JToggleButton> buttonCarMap = new HashMap<>();
	private Map<Integer, JLabel> labelCarMap = new HashMap<>();
	private transient Map<Integer, BufferedImage> carIcons = new HashMap<>();

	// Others generic visual elements, Informations, Button for Launch Cars and
	// Circular Slide to change the Course of Cars.
	private JButton buttonCarLaunch;
	private JLabel carLimitReached = new JLabel("Reached Max # of Cars.");
	private JKnob cslider = new JKnob();
	private JLabel csliderValue = new JLabel("C:0\u00B0");
	private JLabel briefInstruction = new JLabel("Select the cars to launch and maneuver");
	private JLabel serverPresence = new JLabel("[SERVER OFF]");
	private static final String TACTICAL_INFO = "Tactical Info";

	/**
	 * Assembly the Panel. Set the properties and listeners for the visual elements
	 */
	public void setupMMIControls() {
		briefInstruction.setBounds(5, 5, 250, 20);
		briefInstruction.setHorizontalAlignment(SwingConstants.LEFT);
		briefInstruction.setVisible(true);
		add(briefInstruction);
		buttonCarLaunch = new JButton(buttonCarLaunchAction());
		buttonCarLaunch.setBounds(30, 90, 80, 20);
		buttonCarLaunch.setHorizontalAlignment(SwingConstants.LEFT);
		buttonCarLaunch.setEnabled(false);
		JSeparator separator = new JSeparator();
		separator.setBounds(TerminalLoader.TERMINALSTARTHORIZONTALPOSITION, 0, 8, TerminalLoader.TERMINALVERTICALSIZE);
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBackground(Color.BLUE);
		setLayout(null);
		add(buttonCarLaunch);
		carLimitReached.setBounds(10, 30, TerminalLoader.TERMINALSTARTHORIZONTALPOSITION, 10);
		carLimitReached.setForeground(Color.RED);
		carLimitReached.setVisible(false);
		add(carLimitReached);
		initializeCarJButtons();
		add(separator);
		cslider.setBounds(120, 60, 80, 88);
		cslider.setEnabled(false);
		csliderValue.setBounds(150, 40, 50, 20);
		csliderValue.setEnabled(false);
		add(cslider);
		add(csliderValue);
		cslider.addMouseMotionListener(csliderMouseMotionListenerAction());
		serverPresence.setBounds(20, TerminalLoader.TERMINALVERTICALSIZE - 40, 250, 20);
		serverPresence.setFont(new Font("Verdana", Font.BOLD, 15));
		serverPresence.setForeground(Color.RED);
		serverPresence.setHorizontalAlignment(SwingConstants.LEFT);
		serverPresence.setVisible(true);
		add(serverPresence);

	}

	private AbstractAction buttonCarLaunchAction() {
		return new AbstractAction("Launch") {
			public void actionPerformed(ActionEvent e) {
				// Until not reaches the limit of cars to be launched:
				if (instanceController.getTotalCarsLaunched() < FleetConfiguration.FLEETCARS.size()) {
					// For each car of the list of cars
					List<Car> listCarToLaunch = new ArrayList<>();
					for (int i = 0; i < FleetConfiguration.FLEETCARS.size(); i++) {
						launchedCarsMapFromController = instanceController.getLaunchedCarsMap();
						if (!launchedCarsMapFromController.containsKey(i)  && buttonCarMap.get(i).isSelected() ) {
							listCarToLaunch.add(FleetConfiguration.FLEETCARS.get(i));
						}
					}
					instanceController.launchCarRequest(listCarToLaunch, cslider.getAngleInDegrees());
					try {
						// Sleep time to avoid to raise a Concurrency Modification Exception (in case of
						// launching all cars at once)
						Thread.sleep(NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_LAUNCHPUBLICATION);
					} catch (InterruptedException e1) {
						LOGGER.error("InterruptedException: Thread Interrupted" + e1);
						Thread.currentThread().interrupt();
					}
				} else {
					LOGGER.info("Max number of Cars already launched.");
					// Raise alert about max cars already launched and disable button.
					carLimitReached.setVisible(true);
					buttonCarLaunch.setVisible(false);
				}
			}
		};
	}

	private MouseMotionListener csliderMouseMotionListenerAction() {
		return new MouseMotionListener() {
			// For mousedragged movements in the circular slider ...
			@Override
			public void mouseDragged(MouseEvent e) {
				csliderValue.setText("C:" +(int) FleetConfiguration.moduleCourse((float) cslider.getAngleInDegrees()) + "\u00B0");
				LOGGER.debug("Setting circular circular to Course off " + cslider.getAngleInDegrees() + "degrees");
				// We will iterate over all the launched cars and..
				launchedCarsMapFromController = instanceController.getLaunchedCarsMap();
				Map<Integer, Car> maneuveredCarsMap  = new HashMap<>();
				int countButtonpressed = 0;
				for (Map.Entry<Integer, Car> entry : launchedCarsMapFromController.entrySet()) {
					Car car = entry.getValue();
					// Each car already launched will be change its Course if the correspondent JToggle button is selected!
					if (buttonCarMap.get( car.getCarId()).isSelected()) {
						maneuveredCarsMap.put(car.getCarId(), car);
						countButtonpressed++;
					}
				}
				if(countButtonpressed > 0) {
					instanceController.setCourseCarRequest(maneuveredCarsMap, (float) cslider.getAngleInDegrees());
				}
			}
			@Override
			public void mouseMoved(MouseEvent e) {
				//TODO: Implement hover feature
			}
		};
	}

	/**
	 * Initializes the JToogleButtons for each car
	 */
	private void initializeCarJButtons() {
		for (int i = 0; i < FleetConfiguration.FLEETCARS.size(); i++) {
			JToggleButton newbuttonCar = new JToggleButton(FleetConfiguration.FLEETCARS.get(i).getCarName());
			JLabel courseLabelCar = new JLabel("XXXd");
			newbuttonCar.setBounds(5, 150 + i * 40, 90, 30);
			courseLabelCar.setBounds(96, 150 + i * 40, 150, 30);
			newbuttonCar.setBackground(Color.LIGHT_GRAY);
			newbuttonCar.setForeground(Color.WHITE);
			courseLabelCar.setForeground(Color.BLACK);
			courseLabelCar.setText(TACTICAL_INFO);
			newbuttonCar.setHorizontalAlignment(SwingConstants.LEFT);
			courseLabelCar.setHorizontalAlignment(SwingConstants.LEFT);
			newbuttonCar.setEnabled(false);
			add(newbuttonCar);
			courseLabelCar.setVisible(true);
			add(courseLabelCar);
			buttonCarMap.put(i, newbuttonCar);
			labelCarMap.put(i, courseLabelCar);
		}
	}

	/**
	 * Update JButtons according server availability
	 * 
	 * @param state The server availability
	 */
	private void updateMMIFromServerAvailability(Boolean state) {
		String serverState;
		Color serverColor;
		if (instanceController.isLastheartBeating() != Boolean.TRUE.equals(state)) {
			if (Boolean.TRUE.equals(state)) {
				serverState = "ON";
				serverColor = Color.BLUE;
			} else {
				serverState = "OFF";
				serverColor = Color.RED;

			}
			serverPresence.setForeground(serverColor);
			serverPresence.setText("[SERVER " + serverState + "]");
			buttonCarLaunch.setEnabled(state);
			csliderValue.setEnabled(state);
			cslider.setEnabled(state);
			for (int i = 0; i < FleetConfiguration.FLEETCARS.size(); i++) {
				JToggleButton buttonCar = buttonCarMap.get(i);
				buttonCar.setEnabled(state);
				if (Boolean.TRUE.equals(state)) 	buttonCar.setBackground(FleetConfiguration.FLEETCARS.get(i).getCarColor());
				else buttonCar.setBackground(Color.LIGHT_GRAY);
			}
			instanceController.setLastheartBeating(state);
		}
	}

	/**
	 * Constructor
	 */
	public TOSDisplay() {
		// Some informations about network configuratios. (Please see the xml file)
		LOGGER.info("controllerWaitingTimeinMillisforLaunchPublication: "
				+ NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_LAUNCHPUBLICATION + NetworkLayerLoader.NETCONFIGINFO);
		LOGGER.info("controllerWaitingTimeinMillisforUpdateTrackList: "
				+ NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_UPDATETACTICALSITUATION + NetworkLayerLoader.NETCONFIGINFO);
		LOGGER.info("controllerWaitingTimeinMillisforHearBeat: "
				+ NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_HEARTBEAT + NetworkLayerLoader.NETCONFIGINFO);
		// Load the Car Icons and ..
		loadCarIcons();
		// Prepare the remaining visual elements.
		setupMMIControls();
	}

	/**
	 * Loads the respective img Icons from each car
	 */
	private void loadCarIcons() {
		for (int i = 0; i < FleetConfiguration.FLEETCARS.size(); i++) {
			try {
				BufferedImage carIcon = ImageIO
						.read(ClassLoader.getSystemResourceAsStream(FleetConfiguration.FLEETCARS.get(i).getCarIconFile()));
				carIcons.put(i, carIcon);
			} catch (IOException e) {
				LOGGER.error("IOException: " + e);

			}
		}
	}

	/**
	 * Set the dimension for the application
	 */
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(TerminalLoader.TERMINALHORIZONTALSIZE, TerminalLoader.TERMINALVERTICALSIZE);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		// Draw the cars in its current position.
		drawCars(g);
	}

	/**
	 * Draw the cars in its current position. If the car is not launched yet: drawn
	 * in its 'garage' (start point, defined in the configuration) If the car is
	 * already launched, drawn it in its updated coordinate. Prints also some
	 * informations about the car, like speed, name, and constantly updates the
	 * information about course and position. (tactical situation)
	 */
	private void drawCars(Graphics g) {
		// For each car from the fleet..
		for (int i = 0; i < FleetConfiguration.FLEETCARS.size(); i++) {
			// .. is is not already launched, print it at the garage and the information
			// about its speed.
			launchedCarsMapFromController = instanceController.getLaunchedCarsMap();
			if (!launchedCarsMapFromController.containsKey(i)) {
				g.drawImage(carIcons.get(i), (int) FleetConfiguration.FLEETCARS.get(i).getLastLongitude(),
						(int) FleetConfiguration.FLEETCARS.get(i).getLastLatitude(), this);
				g.drawString("   Speed: " + FleetConfiguration.FLEETCARS.get(i).getLastSpeed(),
						(int) FleetConfiguration.FLEETCARS.get(i).getLastLongitude(),
						(int) FleetConfiguration.FLEETCARS.get(i).getLastLatitude()
								+ FleetConfiguration.FLEETCARS.get(i).getCarVerticalSize() + 10);

			}
		}
		// For each car already launched..
		launchedCarsMapFromController = instanceController.getLaunchedCarsMap();
		for (Map.Entry<Integer, Car> entry : launchedCarsMapFromController.entrySet()) {
			Car car = entry.getValue();
			g.setColor(car.getCarColor());
			LOGGER.debug("Color = " + car.getCarColor());
			// Update its position and puts the name of the car below its Icon.
			g.drawImage(carIcons.get(car.getCarId()), (int) car.getLastLongitude(), (int) car.getLastLatitude(), this);
			g.drawString(car.getCarName(), (int) car.getLastLongitude(),
					(int) car.getLastLatitude() + car.getCarVerticalSize() + TerminalLoader.CARMARGIN);
		}

	}

	/**
	 * Main Class
	 */
	public static void main(String[] args) {
		instanceController.setBrokerConfirmed(true);
		instanceController.run();
		TOSDisplay.app();
	}

	/**
	 * App method, just call the SwingUtilities
	 */
	public static void app() {
		SwingUtilities.invokeLater(() -> new TOSDisplay().go());
	}

	// Subscriber Thread to receive the handler start flag from Async message.
	transient Thread subscriberHandlerStartThread = new Thread(this::startHandlerSubscriber);

	private void startHandlerSubscriber() {
		// Subscriber for Handler Start Async message
		SubscriberHandlerStart subscriberHandlerStart = new SubscriberHandlerStart();
		try {
			subscriberHandlerStart.connect(brokerIP);
			subscriberHandlerStart.getMessageConsumer().setMessageListener((Message message) -> {
					LOGGER.debug("onMessage " + message);
					if (message instanceof ObjectMessage) {
						ObjectMessage objMsg = (ObjectMessage) message;
						try {
							Boolean started = (Boolean) objMsg.getObject();
							if(Boolean.TRUE.equals(started)){
								LOGGER.debug("Server Started!");
								for (int i = 0; i < FleetConfiguration.FLEETCARS.size(); i++) {
									buttonCarMap.get(i).setSelected(false);
									labelCarMap.get(i).setText(TACTICAL_INFO);
								}
								instanceController.setTotalCarsLaunched(0);
								buttonCarLaunch.setVisible(true);
								carLimitReached.setVisible(false);
							}
						} catch (JMSException e) {
							LOGGER.error("Exception receiving subscriberHandlerStart msg" + e);
						}
					}
			});
		} catch (JMSException e) {
			LOGGER.error("Exception receiving subscriberHandlerStart msg" + e);
		}
	}

	// Subscriber Thread to receive the car list updated from Async message.
	transient Thread subscriberUpdateCarListThread = new Thread(this::subscribeUpdateCarList);

	private void subscribeUpdateCarList() {
		// Subscriber for Update Track List Async message
		SubscriberUpdateCarList subscriberUpdateCarList = new SubscriberUpdateCarList();
		try {
			subscriberUpdateCarList.connect(brokerIP);
			subscriberUpdateCarList.getMessageConsumer().setMessageListener((Message message) -> {
					LOGGER.debug("onMessage " + message);
					if (message instanceof ObjectMessage) {
						ObjectMessage objMsg = (ObjectMessage) message;
						try {
							Map<Integer, Car> launchedCarsMapFromHandler = (Map<Integer, Car>) objMsg.getObject();
							instanceController.setLaunchedCarsMap(launchedCarsMapFromHandler);
							instanceController.setTotalCarsLaunched(launchedCarsMapFromHandler.size());
							LOGGER.debug(
									"Car List Updated! Contain " + launchedCarsMapFromHandler.size() + " elements");
						} catch (JMSException e) {
							LOGGER.error("JMSExcepton consuming UpdateCarList message" + e);
						}
					}
			});
		} catch (JMSException e) {
			LOGGER.error("JMSException: " + e);
		}
	}

	/**
	 * Assembly the frame and start it
	 */
	public void go() {
		JFrame frame = new JFrame(
				"Terminal Operation System Challenge - A Project-Challenge for Fernando Jose Capeletto Neto application to TBA Group");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.getContentPane().add(this);
		frame.setResizable(false);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		Thread thr = new Thread(this);
		thr.start();
		subscriberHandlerStartThread.start();
		subscriberUpdateCarListThread.start();
	}

	/**
	 * Called inside a infinite loop, for each cycle update the tactical information
	 * and control the alerts and button for launching cars.
	 */
	public void redraw() {

		if (instanceController.isHeartBeating()) {
			updateMMIFromServerAvailability(true);
			try {
				Thread.sleep(NetworkLayerLoader.CONTROLLER_DELAYTIME_INMILLIS_FOR_LAUNCHPUBLICATION);
			} catch (InterruptedException e) {
				LOGGER.error("InterruptedException:" + e);
				Thread.currentThread().interrupt();
			}
			launchedCarsMapFromController = instanceController.getLaunchedCarsMap();

			if (instanceController.getTotalCarsLaunched() == FleetConfiguration.FLEETCARS.size()) {
				buttonCarLaunch.setVisible(false);
				carLimitReached.setVisible(true);
			}
			for (Map.Entry<Integer, Car> entry : launchedCarsMapFromController.entrySet()) {
				Car car = entry.getValue();
				int carId = car.getCarId();
				labelCarMap.get(carId)
						.setText("C:" + (int) car.getLastCourse() + "\u00B0" + " P:(" + (int) car.getLastLatitude() + ","
								+ (int) car.getLastLongitude() + ") S:" + (int) car.getLastSpeed());
				labelCarMap.get(carId).setVisible(true);

			}
			if(launchedCarsMapFromController.size() == 0) {
				for (int i = 0; i < FleetConfiguration.FLEETCARS.size(); i++) {
					labelCarMap.get(i).setText(TACTICAL_INFO);
				}
			}
		} else {
			updateMMIFromServerAvailability(false);
		}
		repaint();
	}

	public void run() {
		while (this.isFocusable()) {
			redraw();
		}

	}
}
