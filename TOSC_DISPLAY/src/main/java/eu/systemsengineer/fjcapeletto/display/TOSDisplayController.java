package eu.systemsengineer.fjcapeletto.display;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import eu.systemsengineer.fjcapeletto.msg.TOSController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.msg.publisher.PublisherLaunchCar;
import eu.systemsengineer.fjcapeletto.msg.publisher.PublisherSetCourseCar;
import eu.systemsengineer.fjcapeletto.msg.subscriber.SubscriberHeartBeat;

/**
 * Class containing the Controller of the Project. 
 * Acts between display and model.
 * Contains the Publisher and Subscriber implementation for the Async Messages. 
 * Consumes the Launch Car and SetCourse Car Async Messages, Update the model and Publish the UpdateCar Async Message constantly.
 * @author Fernando Jose Capeletto Neto
 */
public class TOSDisplayController extends TOSController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TOSDisplayController.class);
	// Singletown Class, only one instance.
	private static final TOSDisplayController instance = new TOSDisplayController();

	//HeartBeat Indicator: False If exceed timer without receive hearbeat message. 
	private boolean heartBeating = false;
	
	//Last HeartBeat Indicator: Last Information about updating (To avoid redundant updates). 
	private boolean lastheartBeating = false;

	// Heart Beat Receptor
	private HeartBeatThread heartbeatThread;
	

	// Used to compute the next position taken into account the Car Speed.
	double deltaX =0 ;
	double deltaY = 0;
	
	// Controls the launched cars
	private Map<Integer, Car> launchedCarsMap = new HashMap<>();
	private int totalCarsLaunched = 0;
	
	/**
	 * Returns the unique instance for the TOSController 
	 * @return instance
	 */
	public static TOSDisplayController getInstance() {
		return instance;
	}
	

	/**
	 * Returns the information if the server is alive 
	 * @return heartBeating The server availability
	 */
	public boolean isHeartBeating() {
		return heartBeating;
	}

	/**
	 * Sets the information that the server is alive 
	 * @param heartBeating the server availability
	 */
	public void setHeartBeating(boolean heartBeating) {
		this.heartBeating = heartBeating;
	}

	/**
	 * Returns the last information if the server is alive 
	 * @return lastheartBeating The last information about server availability
	 */
	public boolean isLastheartBeating() {
		return lastheartBeating;
	}

	/**
	 * Sets the last information received from server  
	 * @param lastheartBeating the last server availability
	 */
	public void setLastheartBeating(boolean lastheartBeating) {
		this.lastheartBeating = lastheartBeating;
	}

	
	/**
	 * Returns the Map for the launched Cars 
	 * @return launchedCarsMap
	 */
	public Map<Integer, Car> getLaunchedCarsMap() {
		return launchedCarsMap;
	}
	
	/**
	 * Sets the Map for the launched Cars 
	 * @param launchedCarsMap the Map of launched Cars to be set
	 */
	public void setLaunchedCarsMap(Map<Integer, Car> launchedCarsMap) {
		this.launchedCarsMap = launchedCarsMap;
	}

	/**
	 * Include a element in the Map for the launched Cars 
	 * @param car The car to be included in the Map
	 */
	public void putLaunchedCarsMap(Car car) {
		this.launchedCarsMap.put(car.getCarId(), car);
	}
	
	/**
	 * Remove a element in the Map for the launched Cars 
	 * @param car The car to be removed from the Map
	 */
	public void dropLaunchedCarsMap(Car car) {
		this.launchedCarsMap.remove(car.getCarId());
	}
	
	
	/**
	 * Returns the total of cars launched 
	 * @return totalCarsLaunched
	 */
	public int getTotalCarsLaunched() {
		return totalCarsLaunched;
	}
	
	/**
	 * Set the total of cars launched 
	 * @param totalCarsLaunched the total of cars launched to be set
	 */
	public void setTotalCarsLaunched(int totalCarsLaunched) {
		this.totalCarsLaunched = totalCarsLaunched;
	}
	
	/**
	 * Constructor   
	 * 
	 */
	private TOSDisplayController() {
		super();
	}

	
	/**
	 * LaunchCar Request message method 
	 * Used from Display side, just prepare the message connectors and topic send the message to launch a car.
 	 * @param	listCarToLaunch List of cars to be launched
 	 * @param	angleInDegrees Angle in Degrees
	 */
	public void launchCarRequest(List<Car> listCarToLaunch, double angleInDegrees) {
		PublisherLaunchCar publisherLaunchCar = new PublisherLaunchCar();
		try {
			publisherLaunchCar.connect(getBrokerIP());
			for (Iterator<Car> iterator = listCarToLaunch.iterator(); iterator.hasNext();) {
				Car car = iterator.next();
				car.addCourse((float) angleInDegrees);
			}
			publisherLaunchCar.publish(listCarToLaunch, angleInDegrees);
			LOGGER.info("publish method requested Car creation for " + listCarToLaunch.size() + "car(s)");
			publisherLaunchCar.closeConnection();
		} catch (JMSException e) {
			LOGGER.error("Exception sending PublisherLaunchCar msg:" + e);
		}
	}

	/**
	 * setCourseCar Request message method 
	 * Used from Display side, just prepare the message connectors and topic send the message to set the course for a car.
 	 * @param	maneuveredCarsMap	The map of cars to be maneuver
 	 * @param	angleInDegrees The new course for the car
	 */
	public void setCourseCarRequest(Map<Integer, Car> maneuveredCarsMap, float angleInDegrees) {
		PublisherSetCourseCar publisherSetCourseCar = new PublisherSetCourseCar();
		try {
			publisherSetCourseCar.connect(getBrokerIP());
			publisherSetCourseCar.publish(maneuveredCarsMap, angleInDegrees);
			LOGGER.info("publish method requested set track course.");
			publisherSetCourseCar.closeConnection();
		} catch (JMSException e) {
			LOGGER.error("Exception sending PublisherSetCourseCar msg:" + e);
		}
	}

	@Override
	public void run() {
		SubscriberHeartBeat subscriberHeartBeat = new SubscriberHeartBeat();
		try {
			// Try to connect to the client and sets the topic for the message
			subscriberHeartBeat.connect(getBrokerIP());
			subscriberHeartBeat.getMessageConsumer().setMessageListener((Message message) -> {
					if (message instanceof ObjectMessage) {
						ObjectMessage objMsg = (ObjectMessage) message;
						try {
							Boolean started = (Boolean) objMsg.getObject();
							if(Boolean.TRUE.equals(started)){
								LOGGER.info("Heart Beat received, Server is present: onMessage" + message);
								if(heartbeatThread != null ){
									heartbeatThread.getHeartBeattimer().cancel();
									heartbeatThread.getHeartBeattimer().purge();
									LOGGER.info("Heart Beat Timer cancelled and purged !");
								}
								setHeartBeating(true);
								heartbeatThread = new HeartBeatThread();
								heartbeatThread.start();
							}
						} catch (JMSException e) {
							LOGGER.error("JMSExcepton consuming HeartBeat message" + e);
						}
					}
			});
		} catch (JMSException e) {
			LOGGER.error("JMSException:" + e);
		}
	}
}
