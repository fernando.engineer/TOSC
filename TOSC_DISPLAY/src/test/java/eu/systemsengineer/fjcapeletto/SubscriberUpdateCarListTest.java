package eu.systemsengineer.fjcapeletto;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.activemq.broker.BrokerService;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.model.CarsBase;
import eu.systemsengineer.fjcapeletto.msg.publisher.PublisherUpdateCarList;
import eu.systemsengineer.fjcapeletto.msg.subscriber.SubscriberUpdateCarList;

public class SubscriberUpdateCarListTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriberUpdateCarListTest.class);
	static ApplicationContext springContext = new ClassPathXmlApplicationContext("FleetConfiguration.xml");
	static CarsBase carsBaseConfig = (CarsBase) springContext.getBean("carsbase-configuration");
	private static PublisherUpdateCarList publisherUpdateTrackList;
	private static SubscriberUpdateCarList subscriberUpdateTrackList;
	private Map<Integer,Car> mapCarList = new HashMap<Integer,Car>();
	public static List<Car> fleetCars =  carsBaseConfig.getFleetCars();
	private static BrokerConfig brokerconfigIntance = BrokerConfig.getInstance();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		brokerconfigIntance.start();

		publisherUpdateTrackList = new PublisherUpdateCarList();
		publisherUpdateTrackList.connect(null);

		subscriberUpdateTrackList = new SubscriberUpdateCarList();
		subscriberUpdateTrackList.connect(null);
		

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		publisherUpdateTrackList.closeConnection();
		subscriberUpdateTrackList.closeConnection();
	}


	@Test
	public void testUpdateTrackList() {
		Map<Integer, Car> maneuveredCarsMap  = new HashMap<Integer, Car>();
		Car car1 = fleetCars.get(3);
		car1.addCourse(34);
		Car car2 = fleetCars.get(5);
		car2.addLatitude(64.3);
		maneuveredCarsMap.put(1, car1);
		maneuveredCarsMap.put(2, car2);
		try {
			publisherUpdateTrackList.publish(maneuveredCarsMap);
			subscriberUpdateTrackList.getMessageConsumer().setMessageListener((Message message) ->  {
					LOGGER.debug("onMessage " + message);
					if (message instanceof ObjectMessage) {
						ObjectMessage objMsg = (ObjectMessage) message;
						try {
							Map<?,?> anotherMapCar = (Map<?,?>) objMsg.getObject();
							Car testCar1 = (Car) anotherMapCar.get(1);
							Car testCar2 = (Car) anotherMapCar.get(2);
							assertEquals(34,testCar1.getLastCourse(),0.001);
							assertEquals(64.3,testCar2.getLastLatitude(),0.001);
						} catch (JMSException e) {
							LOGGER.error("JMSException:" + e);
						}
					}
			});
		} catch (JMSException e) {
			LOGGER.error("JMSException:" + e);
		}
		
		
	}
}