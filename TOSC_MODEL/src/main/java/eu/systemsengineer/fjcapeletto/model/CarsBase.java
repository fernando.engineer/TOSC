package eu.systemsengineer.fjcapeletto.model;

import java.util.List;


/**
 * Interface for CarBase Implementations.
 * @author Fernando Jose Capeletto Neto
 */


public interface CarsBase {

	
	
	/**
	 * Returns the max numbers of cars for the Terminal. 
	 * @return max numbers of cars for the Terminal.
	 */
	public int getMaxCars();
	
	
	/**
	 * Set the max numbers of cars for the Terminal. 
	 * Used for Spring Injection.
	 * @param	maxCars	max numbers of cars for the Terminal.
	 */
	public void setMaxCars(int maxCars);
	
	
	/**
	 * Set the fleet of cars for the Terminal. 
	 * Used for Spring Injection.
	 * @param carCollection	a list of cars
	 */
	public void setFleetCars(List<Car> carCollection);

	
	/**
	 * Returns the collection of cars representing the fleet of cars for the Terminal. 
	 * @return	a list of cars
	 */
	public List<Car> getFleetCars();
	
}
