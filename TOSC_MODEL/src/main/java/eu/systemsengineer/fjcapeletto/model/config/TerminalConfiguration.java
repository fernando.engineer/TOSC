package eu.systemsengineer.fjcapeletto.model.config;


/**
 * Class containing the properties for the Display Application.
 * Configured through Spring Injection by the bean 'display-configuration'  
 * @author Fernando Jose Capeletto Neto
 */

public class TerminalConfiguration {
	
	private int terminalHorizontalSize;
	private int terminalVerticalSize;
	private int terminalStartPosition;
	private int carMargin;
	private int terminalBorder;
	
	
	/**
	 * Returns Horizontal Size for the Terminal. 
	 * @return Horizontal Size for the Terminal.
	 */
	public int getTerminalHorizontalSize() {
		return this.terminalHorizontalSize;
	}

	/**
	 * Set the Horizontal Size for the Terminal. 
	 * Used for Spring Injection.
	 * @param terminalHSize  Horizontal Size for the Terminal.
	 */	
	public void setTerminalHorizontalSize(int terminalHSize) {
		this.terminalHorizontalSize = terminalHSize;
	}
	
	/**
	 * Returns Vertical Size for the Terminal. 
	 * @return Vertical Size for the Terminal.
	 */	
	public int getTerminalVerticalSize() {
		return this.terminalVerticalSize;
	}

	/**
	 * Set the Vertical Size for the Terminal. 
	 * Used for Spring Injection.
	 * @param  terminalVHSize  Vertical Size for the Terminal.
	 */
	public void setTerminalVerticalSize(int terminalVHSize) {
		this.terminalVerticalSize = terminalVHSize;
	}
	
	/**
	 * Returns Horizontal start position where the Cars will run.
	 * The area before this position is reserved for the left menu. 
	 * @return Horizontal start position where the Cars will run.
	 */
	public int getTerminalStartPosition() {
		return terminalStartPosition;
	}

	/**
	 * Set the Horizontal start position where the Cars will run.
	 * The area before this position is reserved for the left menu. 
 * 	 * Used for Spring Injection.
	 * @param terminaStartPosition Horizontal start position where the Cars will run.
	 */
	public void setTerminalStartPosition(int terminaStartPosition) {
		this.terminalStartPosition = terminaStartPosition;
	}

	/**
	 * Returns the margin of distance to be respected from a given car, to be displayed its name on display during movements.
	 * @return Margin of distance from a car.
	 */
	public int getCarMargin() {
		return carMargin;
	}

	/**
	 * Set the margin of distance to be respected from a given car, to be displayed its name on display during movements.
	 * Used for Spring Injection.
	 * @param carMargin Margin of distance from a car.
	 */
	public void setCarMargin(int carMargin) {
		this.carMargin = carMargin;
	}
	
	/**
	 * Returns the margin of distance to be respected from a terminal border, to a given car.
	 * @return Margin of distance from a terminal border.
	 */	
	public int getTerminalBorder() {
		return terminalBorder;
	}

	/**
	 * Set the margin of distance to be respected from a terminal border, to a given car.
	 * Used for Spring Injection.
	 * @param terminalBorder Margin of distance from a terminal border.
	 */	
	public void setTerminalBorder(int terminalBorder) {
		this.terminalBorder = terminalBorder;
	}
}
