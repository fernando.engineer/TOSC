package eu.systemsengineer.fjcapeletto.model.impl;

import java.awt.Color;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import eu.systemsengineer.fjcapeletto.model.Car;


/**
 * Class containing the model of the Car Object.
 * @author Fernando Jose Capeletto Neto
 */
public class CarImpl implements Car, Serializable {

	
	// Need to be serializable to be sent as activemq message.
	public static final long serialVersionUID = 1L; // Perhaps to comment

	private int carId;

	// Store the amount of measures for future use (for instance record and replay functions in future versions..)
	private int indexPosition = 0;

	// For the same reason I use a Map to store the tactical history  
	private Map<Integer,Double> latitudes = new HashMap<>();
	private Map<Integer,Double> longitudes = new HashMap<>();
	private Map<Integer,Float> courses = new HashMap<>();
	private Map<Integer,Float> speeds = new HashMap<>();
	
	// Last tactical information about the Car	
	// Obs: Here we are assuming a fictitious understanding about 'latitude' and 'longitude' terms.
	private double lastLatitude;
	private double lastLongitude;
	private float lastCourse;
	private float lastSpeed;

	// Generic properties, including the information about the car Icon
	private Color carColor;
	private String carName;
	private String carIconFile;
	private int carVerticalSize;
	
	// Position where the car shall be at the beggining.
	private double parkingLatitude;
	private double parkingLongitude;

	
	/**
	 * Constructor (Default with no args just for Spring purposes)  
	 * 
	 */	
	public CarImpl() {
		this.carId = -1;
	}
	

	/**
	 * Constructor  
	 * @param id The id for the Car
	 */	
	public CarImpl(int id) {
		this.carId = id;
	}
	/**
	 * Constructor  
	 * @param carId The id for the Car
	 * @param bornLatitude The first Latitude for the Car
	 * @param bornLongitute The first Longitude for the Car
	 * @param angleInDegrees The first Course for the Car
	 * @param carColor The color of the Car
	 */
	public CarImpl(int carId, double bornLatitude, double bornLongitute, double angleInDegrees, Color carColor) {
		this.carId = carId;
		addLatitude(bornLatitude);
		addLongitude(bornLongitute);
		addCourse((float) angleInDegrees);
		setCarColor(carColor);
	}
	
	/**
	 * Returns the Car Id 
	 * @return Car Id
	 */
	public int getCarId() {
		return this.carId;
	}

	/**
	 * Set the Car Id 
	 * Used for Spring Injection.
	 * @param	id CarImpl Id
	 */
	public void setCarId(int id) {
		this.carId = id;
	}

	/**
	 * Returns the Car Name 
	 * @return Car Name
	 */
	public String getCarName() {
		return carName;
	}

	/**
	 * Set the Car Name 
	 * Used for Spring Injection.
	 * @param	carName CarImpl Name
	 */
	public void setCarName(String carName) {
		this.carName = carName;
	}

	/**
	 * Returns the path of Car Icon File 
	 * @return Path for Car Icon File
	 */
	public String getCarIconFile() {
		return carIconFile;
	}

	/**
	 * Set the path of Car Icon File 
	 * Used for Spring Injection.
	 * @param	carIconFile Path for Car Icon File
	 */
	public void setCarIconFile(String carIconFile) {
		this.carIconFile = carIconFile;
	}
	
	/**
	 * Returns the Vertical Size of the Car Icon File
	 * This information shall be acquired from the Icon File 
	 * @return Vertical Size of the Car Icon File 
	 */
	public int getCarVerticalSize() {
		return carVerticalSize;
	}

	/**
	 * Set the Vertical Size of Car Icon File 
	 * This information shall be acquired from the Icon File 
	 * Used for Spring Injection.
	 * @param	carVerticalSize Path for Car Icon File
	 */
	public void setCarVerticalSize(int carVerticalSize) {
		this.carVerticalSize = carVerticalSize;
	}

	/**
	 * Returns the Color of the Car
	 * @return Color of the Car 
	 */
	public Color getCarColor() {
		return carColor;
	}

	/**
	 * Set the Color of the Car
	 * Used for Spring Injection.
	 * @param carColor	Color of the Car
	 */
	public void setCarColor(Color carColor) {
		this.carColor = carColor;
	}

	
	/**
	 * Returns the initial Latitude for the Car
	 * @return initial Latitude for the Car
	 */
	public double getParkingLatitude() {
		return parkingLatitude;
	}

	/**
	 * Set the initial Latitude for the Car
	 * Used for Spring Injection.
	 * @param parkingLatitude initial Latitude for the Car
	 */
	public void setParkingLatitude(double parkingLatitude) {
		this.parkingLatitude = parkingLatitude;
	}
	
	/**
	 * Returns the initial Longitude for the Car
	 * @return initial Longitude for the Car
	 */
	public double getParkingLongitude() {
		return parkingLongitude;
	}

	/**
	 * Set the initial Longitude for the Car
	 * Used for Spring Injection.
	 * @param parkingLongitude initial Longitude for the Car
	 */
	public void setParkingLongitude(double parkingLongitude) {
		this.parkingLongitude = parkingLongitude;
	}
	
	/**
	 * Returns the last Latitude for the Car
	 * @return last Latitude for the Car
	 */
	public double getLastLatitude() {
		return this.lastLatitude;
	}

	/**
	 * Set the last Latitude for the Car
	 * Used for Spring Injection.
	 * @param lat last Latitude for the Car
	 */
	public void setLastLatitude(double lat) {
		this.lastLatitude = lat;
	}
	
	/**
	 * Returns the last Longitude for the Car
	 * @return last Longitude for the Car
	 */
	public double getLastLongitude() {
		return this.lastLongitude;
	}

	/**
	 * Set the last Longitude for the Car
	 * Used for Spring Injection.
	 * @param longt last Longitude for the Car
	 */
	public void setLastLongitude(double longt) {
		this.lastLongitude = longt;
	}
	
	/**
	 * Returns the last Course for the Car
	 * @return last Course for the Car
	 */
	public float getLastCourse() {
		return this.lastCourse;
	}

	/**
	 * Set the last Course for the Car
	 * Used for Spring Injection.
	 * @param course last Course for the Car
	 */
	public void setLastCourse(float course) {
		this.lastCourse = course;
	}

	/**
	 * Returns the last Speed for the Car
	 * @return last Speed for the Car
	 */
	public float getLastSpeed() {
		return this.lastSpeed;
	}
	
	/**
	 * Set the last Speed for the Car
	 * Used for Spring Injection.
	 * @param speed last Speed for the Car
	 */
	public void setLastSpeed(float speed) {
		this.lastSpeed = speed;
	}

	/**
	 * Add a new Latitude for the Car and Set it as its new Last Latitude
	 * @param lat new Latitude for the Car
	 */
	public void addLatitude(double lat) {
		latitudes.put(indexPosition,lat);
		setLastLatitude(lat);
	}

	/**
	 * Add a new Longitude for the Car and Set it as its new Last Longitude
	 * @param longt new Longitude for the Car
	 */
	public void addLongitude(double longt) {
		longitudes.put(indexPosition,longt);
		setLastLongitude(longt);
	}

	/**
	 * Add a new Course for the Car and Set it as its new Last Course
	 * @param courseInDegrees new Course for the Car
	 */
	public void addCourse(float courseInDegrees) {
		courseInDegrees = moduleCourse(courseInDegrees);
		courses.put(indexPosition,courseInDegrees);
		setLastCourse(courseInDegrees);
	}

	/**
	 * Add a new Speed for the Car and Set it as its new Last Speed
	 * @param speed new Speed for the Car
	 */
	public void addSpeed(float speed) {
		speeds.put(indexPosition,speed);
		setLastSpeed(speed);
	}

	/**
	 * Add a new Measure for the Car
	 */
	public void addMeasure() {
		this.indexPosition++;
	}
	
	public static float moduleCourse(float courseInDegrees) {
		if(courseInDegrees < 0) {
			courseInDegrees += 360;
		}
		if (courseInDegrees > 360) {
			courseInDegrees -= 360;
		}
		return courseInDegrees;
	}

}
