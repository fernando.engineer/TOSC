package eu.systemsengineer.fjcapeletto.model.config;

import eu.systemsengineer.fjcapeletto.model.Car;
import eu.systemsengineer.fjcapeletto.model.CarsBase;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class FleetConfiguration {

    // Beans Configurations
    // XML Configuration Files and its Config Beans
    private static ApplicationContext springContext = new ClassPathXmlApplicationContext("FleetConfiguration.xml");
    private static CarsBase carsBaseConfig = (CarsBase) springContext.getBean("carsbase-configuration");

    // Concentrates all the properties injected by beans in utilities attributes ...


    // .. for Cars Base Configurations
    public static final int MAXCARS = carsBaseConfig.getMaxCars();
    public static final List<Car> FLEETCARS =  carsBaseConfig.getFleetCars();

    // I developed this method to protect the app if the user disrespect the rules at the xml level
    // the last elements will be dropped! But I start to test this method and not works in all the situations.
    // So due to the time limit, for now the rules in the XML shall be followed!
    /**
     * Verifies if the fleet of Cars have more then the max allowed number for the Terminal and drop the last elements until back for the limit.
     */
    public static void checkMaxCarsAllowed() {
        while(FLEETCARS.size() > MAXCARS) {
            FLEETCARS.remove(FLEETCARS.size()-1);
        }
    }

    /**
     * Normalizes the course in degrees to keep it in the interval [0,360[
     * @param courseInDegrees The angle in degrees, to be normalized
     * @return courseInDegrees The angle in degrees, normalized
     */
    public static float moduleCourse(float courseInDegrees) {
        if(courseInDegrees < 0) {
            courseInDegrees += 360;
        }
        if (courseInDegrees > 360) {
            courseInDegrees -= 360;
        }
        return courseInDegrees;
    }

}
