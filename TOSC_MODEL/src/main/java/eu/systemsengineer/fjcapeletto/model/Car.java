package eu.systemsengineer.fjcapeletto.model;

import java.awt.Color;

/**
 * Interface for Car Implementations.
 * @author Fernando Jose Capeletto Neto
 */


public interface Car {
	
	// Need to be serializable to be sent as activemq message.
	public static final long serialVersionUID = 1L;
	
	/**
	 * Returns the Car Id 
	 * @return Car Id
	 */
	public int getCarId();
	
	
	/**
	 * Set the Car Id 
	 * Used for Spring Injection.
	 * @param id CarImpl Id
	 */
	public void setCarId(int id);

	/**
	 * Returns the Car Name 
	 * @return Car Name
	 */
	public String getCarName();

	/**
	 * Set the Car Name 
	 * Used for Spring Injection.
	 * @param carName CarImpl Name
	 */
	public void setCarName(String carName);

	/**
	 * Returns the path of Car Icon File 
	 * @return Path for Car Icon File
	 */
	public String getCarIconFile();

	/**
	 * Set the path of Car Icon File 
	 * Used for Spring Injection.
	 * @param	carIconFile Path for Car Icon File
	 */
	public void setCarIconFile(String carIconFile);
	
	/**
	 * Returns the Vertical Size of the Car Icon File
	 * This information shall be acquired from the Icon File 
	 * @return Vertical Size of the Car Icon File 
	 */
	public int getCarVerticalSize();

	/**
	 * Set the Vertical Size of Car Icon File 
	 * This information shall be acquired from the Icon File 
	 * Used for Spring Injection.
	 * @param	carVerticalSize Path for Car Icon File
	 */
	public void setCarVerticalSize(int carVerticalSize);

	/**
	 * Returns the Color of the Car
	 * @return Color of the Car 
	 */
	public Color getCarColor();

	/**
	 * Set the Color of the Car
	 * Used for Spring Injection.
	 * @param carColor	Color of the Car
	 */
	public void setCarColor(Color carColor);

	
	/**
	 * Returns the initial Latitude for the Car
	 * @return initial Latitude for the Car
	 */
	public double getParkingLatitude();

	/**
	 * Set the initial Latitude for the Car
	 * Used for Spring Injection.
	 * @param parkingLatitude initial Latitude for the Car
	 */
	public void setParkingLatitude(double parkingLatitude);
	
	/**
	 * Returns the initial Longitude for the Car
	 * @return initial Longitude for the Car
	 */
	public double getParkingLongitude();

	/**
	 * Set the initial Longitude for the Car
	 * Used for Spring Injection.
	 * @param parkingLongitude initial Longitude for the Car
	 */
	public void setParkingLongitude(double parkingLongitude);
	
	/**
	 * Returns the last Latitude for the Car
	 * @return last Latitude for the Car
	 */
	public double getLastLatitude();

	/**
	 * Set the last Latitude for the Car
	 * Used for Spring Injection.
	 * @param lat last Latitude for the Car
	 */
	public void setLastLatitude(double lat);
	
	/**
	 * Returns the last Longitude for the Car
	 * @return last Longitude for the Car
	 */
	public double getLastLongitude();

	/**
	 * Set the last Longitude for the Car
	 * Used for Spring Injection.
	 * @param longt last Longitude for the Car
	 */
	public void setLastLongitude(double longt);
	
	/**
	 * Returns the last Course for the Car
	 * @return last Course for the Car
	 */
	public float getLastCourse();

	/**
	 * Set the last Course for the Car
	 * Used for Spring Injection.
	 * @param course last Course for the Car
	 */
	public void setLastCourse(float course);

	/**
	 * Returns the last Speed for the Car
	 * @return last Speed for the Car
	 */
	public float getLastSpeed();
	
	/**
	 * Set the last Speed for the Car
	 * Used for Spring Injection.
	 * @param speed last Speed for the Car
	 */
	public void setLastSpeed(float speed);

	/**
	 * Add a new Latitude for the Car and Set it as its new Last Latitude
	 * @param lat new Latitude for the Car
	 */
	public void addLatitude(double lat);

	/**
	 * Add a new Longitude for the Car and Set it as its new Last Longitude
	 * @param longt new Longitude for the Car
	 */
	public void addLongitude(double longt);

	/**
	 * Add a new Course for the Car and Set it as its new Last Course
	 * @param courseInDegrees new Course for the Car
	 */
	public void addCourse(float courseInDegrees);

	/**
	 * Add a new Speed for the Car and Set it as its new Last Speed
	 * @param speed new Speed for the Car
	 */
	public void addSpeed(float speed);

	/**
	 * Add a new Measure for the Car
	 */
	public void addMeasure();

}
