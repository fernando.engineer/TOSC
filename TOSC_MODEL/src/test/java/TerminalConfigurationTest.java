import eu.systemsengineer.fjcapeletto.model.config.TerminalConfiguration;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertEquals;

public class TerminalConfigurationTest {

    static ApplicationContext springContext = new ClassPathXmlApplicationContext("TerminalConfiguration.xml");
    public static TerminalConfiguration terminalConfig = (TerminalConfiguration) springContext.getBean("terminal-configuration");

    @Test
    public void testTerminalDimensions() {
        assertEquals(1200, terminalConfig.getTerminalHorizontalSize());
        assertEquals(600, terminalConfig.getTerminalVerticalSize());
        assertEquals(250, terminalConfig.getTerminalStartPosition());
        assertEquals(15, terminalConfig.getTerminalBorder());
        assertEquals(15, terminalConfig.getCarMargin());
    }

}
